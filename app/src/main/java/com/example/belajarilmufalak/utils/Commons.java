package com.example.belajarilmufalak.utils;

public class Commons {
    public static final String DATABASE_NAME = "fasik";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "login";
    public static final String ID = "id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
}
