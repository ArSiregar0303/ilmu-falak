package com.example.belajarilmufalak.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.belajarilmufalak.views.model.UserModel;

import static com.example.belajarilmufalak.utils.Commons.DATABASE_NAME;
import static com.example.belajarilmufalak.utils.Commons.DATABASE_VERSION;
import static com.example.belajarilmufalak.utils.Commons.ID;
import static com.example.belajarilmufalak.utils.Commons.PASSWORD;
import static com.example.belajarilmufalak.utils.Commons.TABLE_NAME;
import static com.example.belajarilmufalak.utils.Commons.USERNAME;

public class SQLiteHelper extends SQLiteOpenHelper {

    public static final String QUERY = " CREATE TABLE " + TABLE_NAME
            + " ( "
            + ID + " INTEGER PRIMARY KEY, "
            + USERNAME + " TEXT, "
            + PASSWORD + " TEXT "
            + " ) ";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public void addUser(UserModel userModel) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(USERNAME, userModel.username);

        values.put(PASSWORD, userModel.password);

        long Insert_Table = db.insert(TABLE_NAME, null, values);
    }

    public UserModel Authentication(UserModel userModel) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{
                        ID, USERNAME, PASSWORD
                }, USERNAME + "=?",
                new String[]{userModel.username},
                null,
                null,
                null);

        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            UserModel userModel1 = new UserModel(cursor.getString(0), cursor.getString(1), cursor.getString(2));

            if (userModel.password.equalsIgnoreCase(userModel1.password)) {
                return userModel1;
            }
        }
        return null;
    }

    public boolean isUsernameExists(String username) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{
                        ID,
                        USERNAME,
                        PASSWORD
                },
                USERNAME + "=?",
                new String[]{username},
                null,
                null,
                null);

        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            return true;
        }
        return false;
    }
}
