package com.example.belajarilmufalak.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class ViewExtension {

    public void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }

    public void hideView(View view) {
        view.setVisibility(View.INVISIBLE);
    }

    public void removeView(View view) {
        view.setVisibility(View.GONE);
    }

    public static final String simpleName = Activity.class.getSimpleName();

}
