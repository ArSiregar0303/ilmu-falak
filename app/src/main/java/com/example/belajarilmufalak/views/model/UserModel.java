package com.example.belajarilmufalak.views.model;

public class UserModel {
    public String id;
    public String username;
    public String password;

    public UserModel(String id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
}
