package com.example.belajarilmufalak.views.main;

import com.example.belajarilmufalak.R;
import com.example.belajarilmufalak.views.model.MenuModel;

import java.util.ArrayList;

public class MenuData {

    private static String[] titleData = {
            "DASAR - DASAR ILMU FALAK",
            "MENENTUKAN ARAH KIBLAT",
            "MELIHAT HILAL",
            "PENANGGALAN KALENDER HIJRIYAH"
    };

    private static int[] imageData = {
            R.drawable.ic_stars,
            R.drawable.ic_qibla,
            R.drawable.ic_moon,
            R.drawable.ic_calendar,

    };

    static ArrayList<MenuModel> getListData() {
        ArrayList<MenuModel> listData = new ArrayList<>();
        for (int position = 0; position < titleData.length; position++) {
            MenuModel menuModel = new MenuModel();
            menuModel.setTitle(titleData[position]);
            menuModel.setImages(imageData[position]);
            listData.add(menuModel);
        }
        return listData;
    }
}
