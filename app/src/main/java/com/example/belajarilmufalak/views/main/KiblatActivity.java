package com.example.belajarilmufalak.views.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.example.belajarilmufalak.databinding.ActivityKiblatBinding;
import com.example.belajarilmufalak.views.model.MateriModel;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class KiblatActivity extends AppCompatActivity {

    private static final String simpleName = IlmuFalakActivity.class.getSimpleName();

    private ActivityKiblatBinding binding;

    private final static String SNACK_SUCCESS = "success";

    private final static String SNACK_ERROR = "error";

     KiblatDatabase kiblatDatabase;

    ArrayList<MateriModel> materiModels = new ArrayList<MateriModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivityKiblatBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setInitView();

        getLifecycle().addObserver(binding.ytPlayer);

        setYoutubeInit();

    }

    private void setInitView() {

        kiblatDatabase = new KiblatDatabase(this);

        materiModels = kiblatDatabase.getDataKiblat();

        MateriModel data = materiModels.get(0);

        binding.tvDescripOne.setText(data.getDescription_one());
    }

    private void setYoutubeInit() {

        binding.ytPlayer.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NotNull YouTubePlayer youTubePlayer) {
                String videoID = "OY-Ys2yCbMA";
                youTubePlayer.loadVideo(videoID, 0);
            }
        });

        binding.back.setOnClickListener(view -> {
            startActivity(new Intent(this, MenuActivity.class));
            finish();
        });
    }
}