package com.example.belajarilmufalak.views.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.example.belajarilmufalak.databinding.ActivityLoginBinding;
import com.example.belajarilmufalak.utils.SQLiteHelper;
import com.example.belajarilmufalak.views.model.UserModel;
import com.google.android.material.snackbar.Snackbar;

import de.mateware.snacky.Snacky;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding binding;

    private SQLiteHelper sqLiteHelper;

    private final static String SNACK_SUCCESS = "success";

    private final static String SNACK_ERROR = "error";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        sqLiteHelper = new SQLiteHelper(this);

        initCreateAccount();

        initViews();
    }

    private void initCreateAccount() {

    }

    private void initViews() {
        binding.btnLogin.setOnClickListener( view -> {
            if (validateData()) {
                String username = binding.etIdHubungkan.getText().toString();
                String password = binding.etPassword.getText().toString();

                UserModel userModel = sqLiteHelper.Authentication(new UserModel(null,username, password));

                if (userModel != null ) {
                    snack(SNACK_SUCCESS, "Berhasil Login !", Snackbar.LENGTH_LONG);
                    startActivity(new Intent(this, MenuActivity.class));
                    finish();
                } else {
                    snack(SNACK_ERROR, "Gagal Login, silahkan coba lagi !!", Snackbar.LENGTH_LONG);
                }

            }
        });

        binding.tvRegister.setOnClickListener( view -> {
            startActivity(new Intent(this, RegisterActivity.class));
            finish();
        });
    }



    public boolean validateData() {
        boolean validate = false;

        String username = binding.etIdHubungkan.getText().toString();
        String password = binding.etPassword.getText().toString();

        if (username.isEmpty()) {
            validate = false;
            binding.etIdHubungkan.setError("Harap isi Username anda!!!");
        } else {
            validate = true;
            binding.etIdHubungkan.setError(null);
        }

        if (password.isEmpty()) {
            validate = false;
            binding.etPassword.setError("Harap isi Password anda!!!");
        } else {
            if (password.length() > 5) {
                validate = true;
                binding.etPassword.setError(null);
            } else {
                validate = false;
                binding.etPassword.setError("Password anda terlalu singkat!!!");
            }
        }
        return validate;
    }

    private void snack(String type, String msg, Integer duration) {
        if (type.equals(SNACK_SUCCESS)) {
            Snacky.builder()
                    .setActivity(this)
                    .setText(msg)
                    .setDuration(duration)
                    .success()
                    .show();

        } else {
            Snacky.builder()
                    .setActivity(this)
                    .setText(msg)
                    .setDuration(duration)
                    .error()
                    .show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        binding = null;

        sqLiteHelper = null;
    }
}