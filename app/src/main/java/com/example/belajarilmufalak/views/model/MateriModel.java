package com.example.belajarilmufalak.views.model;

public class MateriModel {

    String description_one;
    String description_two;

    public MateriModel(String description_one, String description_two) {
        this.description_one = description_one;
        this.description_two = description_two;
    }

    public String getDescription_one() {
        return description_one;
    }

    public void setDescription_one(String description_one) {
        this.description_one = description_one;
    }

    public String getDescription_two() {
        return description_two;
    }

    public void setDescription_two(String description_two) {
        this.description_two = description_two;
    }
}
