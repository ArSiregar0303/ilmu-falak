package com.example.belajarilmufalak.views.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.example.belajarilmufalak.databinding.ActivityMenuBinding;
import com.example.belajarilmufalak.views.adapter.MenuActivityAdapter;
import com.example.belajarilmufalak.views.adapter.MenuViewPagerAdapter;
import com.example.belajarilmufalak.views.model.MenuModel;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import de.mateware.snacky.Snacky;

public class MenuActivity extends AppCompatActivity {

    private ActivityMenuBinding binding;

    private ViewPager viewPager;

    private WormDotsIndicator wormDotsIndicator;

    private MenuActivityAdapter menuActivityAdapter;

    private MenuViewPagerAdapter menuViewPagerAdapter;

    private LayoutInflater layoutInflater;

    private SharedPreferences sharedPreferences;

    private SharedPreferences.Editor editor;

    private ArrayList<MenuModel> list = new ArrayList<>();

    int currentPage = 0;

    Timer timer;

    final long DELAY_MS = 500;

    final long PERIOD_MS = 3000;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivityMenuBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        initRecyclerView();
        viewPagerState();
        transitionMenu();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void transitionMenu() {

        binding.btnHome.setOnClickListener(view -> {
            TransitionManager.beginDelayedTransition(binding.bnvFake1);
            binding.tvHome.setVisibility(View.VISIBLE);
            binding.tvExit.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (binding.tvHome.getVisibility() == View.VISIBLE) {
                        recreate();
                    }
                }
            }, 600);
        });

        binding.btnExit.setOnClickListener(view -> {
            TransitionManager.beginDelayedTransition(binding.bnvFake1);
            binding.tvHome.setVisibility(View.GONE);
            binding.tvExit.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (binding.tvExit.getVisibility() == View.VISIBLE) {

                        try {

                            if (sharedPreferences == null) {
                                sharedPreferences = getSharedPreferences("prefs", MODE_PRIVATE);

                                editor = sharedPreferences.edit();
                                editor.remove("firstLogin");
                                editor.apply();

                                startActivity(new Intent(MenuActivity.this, LoginActivity.class));
                                finish();
                            }

                        } catch (Exception ex) {
                            Snacky.builder()
                                    .setActivity(MenuActivity.this)
                                    .setText(ex.getMessage())
                                    .setDuration(Snacky.LENGTH_LONG)
                                    .error()
                                    .show();
                        }
                    }
                }
            }, 600);


        });

        binding.fab.setOnClickListener( view -> {
            startActivity(new Intent(this, TentangActivity.class));
            finish();
        });

    }

    private void initRecyclerView() {
        binding.rvMenu.setLayoutManager(new GridLayoutManager(this, 2));
        list.addAll(MenuData.getListData());
        menuActivityAdapter = new MenuActivityAdapter(list);
        binding.rvMenu.setAdapter(menuActivityAdapter);
    }

    private void viewPagerState() {
        viewPager = binding.vpImageHome;
        wormDotsIndicator = binding.wormDotsIndicator;
        menuViewPagerAdapter = new MenuViewPagerAdapter(layoutInflater);
        viewPager.setAdapter(menuViewPagerAdapter);
        wormDotsIndicator.setViewPager(viewPager);
        AutomaticSlide();
    }

    private void AutomaticSlide() {
        final Handler handler = new Handler();
        final Runnable runnable = () -> {
            if (currentPage == menuViewPagerAdapter.getCount()) {
                currentPage = -1;
            }
            viewPager.setCurrentItem(currentPage++, true);
        };

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, DELAY_MS, PERIOD_MS);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}