package com.example.belajarilmufalak.views.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.belajarilmufalak.R;
import com.example.belajarilmufalak.databinding.ActivitySplashScreenBinding;
import com.example.belajarilmufalak.views.adapter.SplashScreenAdapter;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

public class SplashScreen extends AppCompatActivity {

    private BottomSheetBehavior sheetBehavior;

    private LinearLayout bottom_sheet;

    private SharedPreferences sharedPreferences;

    private SharedPreferences.Editor editor;

    public  Boolean firstInstall;

    public Boolean firstLogin;

    private ActivitySplashScreenBinding binding;

    private ViewPager viewPager;

    private WormDotsIndicator wormDotsIndicator;

    private SplashScreenAdapter splashScreenAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivitySplashScreenBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        bottom_sheet = findViewById(R.id.welcome_fragment4);
        viewPager = findViewById(R.id.vp_welcome);

        initialShared();

        checkedState();
    }

    private void initialShared() {

        sharedPreferences = getSharedPreferences("prefs", MODE_PRIVATE);
        firstInstall = sharedPreferences.getBoolean("firstInstall", true);
        firstLogin = sharedPreferences.getBoolean("firstLogin", true);
    }

    private void checkedState() {
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        sheetBehavior.setDraggable(false);
        sheetBehavior.setSkipCollapsed(true);

        if (firstInstall) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            viewPagerState();
        } else {
            if (firstLogin) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                Delays();
            } else {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                firstLoginActivity();
            }
        }

        binding.tvNextWelcome.setOnClickListener(view -> {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            Delays();
        });

        sharedPreferences = getSharedPreferences("prefs", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putBoolean("firstInstall", false);
        editor.putBoolean("firstLogin", false);
        editor.apply();

    }

    private void viewPagerState() {
        viewPager = binding.vpWelcome;
        wormDotsIndicator = binding.wormDotsIndicator;
        splashScreenAdapter = new SplashScreenAdapter(getLayoutInflater());

        viewPager.setAdapter(splashScreenAdapter);
        wormDotsIndicator.setViewPager(viewPager);

        binding.tvNextWelcome.setVisibility(View.GONE);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    firstInstall();
                    binding.tvNextWelcome.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void firstInstall() {
        editor.putBoolean("firstInstall", false);
        editor.apply();
    }

    private void Delays() {
       new Handler().postDelayed(() -> {
           startActivity(new Intent(SplashScreen.this, LoginActivity.class));
           finish();
       }, 5000);
    }

    private void firstLoginActivity() {
        new Handler().postDelayed(() -> {
            startActivity(new Intent(SplashScreen.this, MenuActivity.class));
            finish();
        }, 5000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewPager = null;
        splashScreenAdapter = null;
    }
}