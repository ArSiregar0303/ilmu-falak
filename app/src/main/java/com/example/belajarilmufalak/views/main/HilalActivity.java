package com.example.belajarilmufalak.views.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.example.belajarilmufalak.R;
import com.example.belajarilmufalak.databinding.ActivityHilalBinding;
import com.example.belajarilmufalak.databinding.ActivityKiblatBinding;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;

import org.jetbrains.annotations.NotNull;

public class HilalActivity extends AppCompatActivity {

    private static final String simpleName = IlmuFalakActivity.class.getSimpleName();

    private ActivityHilalBinding binding;

    private final static String SNACK_SUCCESS = "success";

    private final static String SNACK_ERROR = "error";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivityHilalBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        getLifecycle().addObserver(binding.ytPlayer);

        setYoutubeInit();
    }

    private void setYoutubeInit() {

        binding.ytPlayer.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NotNull YouTubePlayer youTubePlayer) {
                String videoID = "O64SV4Au43w";
                youTubePlayer.loadVideo(videoID, 0);
            }
        });

        binding.back.setOnClickListener(view -> {
            startActivity(new Intent(this, MenuActivity.class));
            finish();
        });
    }
}