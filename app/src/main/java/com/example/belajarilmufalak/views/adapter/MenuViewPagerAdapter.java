package com.example.belajarilmufalak.views.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.example.belajarilmufalak.R;
import org.jetbrains.annotations.NotNull;

public class MenuViewPagerAdapter extends PagerAdapter {
    int [] dataImage = new int[] {
            R.drawable.images_one,
            R.drawable.images_two,
            R.drawable.images_three,
            R.drawable.images_four,
            R.drawable.images_five,
            R.drawable.images_six,
    };

    LayoutInflater layoutInflater;

    public MenuViewPagerAdapter(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }

    @Override
    public int getCount() {
        return dataImage.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull @NotNull View view, @NonNull @NotNull Object object) {
        return view == object;
    }

    @NonNull
    @NotNull
    @Override
    public Object instantiateItem(@NonNull @NotNull ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.viewpager_home_item, container, false);
        ImageView imageList = (ImageView) view.findViewById(R.id.iv_imageList);
        imageList.setImageResource(dataImage[position]);
        container.addView(view);
        return view;
    }



    @Override
    public void destroyItem(@NonNull @NotNull ViewGroup container, int position, @NonNull @NotNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
