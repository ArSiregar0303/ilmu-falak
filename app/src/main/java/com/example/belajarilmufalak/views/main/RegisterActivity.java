package com.example.belajarilmufalak.views.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.example.belajarilmufalak.databinding.ActivityRegisterBinding;
import com.example.belajarilmufalak.utils.SQLiteHelper;
import com.example.belajarilmufalak.views.model.UserModel;
import com.google.android.material.snackbar.Snackbar;

import de.mateware.snacky.Snacky;

public class RegisterActivity extends AppCompatActivity {

    private ActivityRegisterBinding binding;

    private SQLiteHelper sqLiteHelper;

    private final static String SNACK_SUCCESS = "success";

    private final static String SNACK_ERROR = "error";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        sqLiteHelper = new SQLiteHelper(this);

        initView();
    }

    private void initView() {
        binding.btnRegister.setOnClickListener(view -> {
            if (validation()) {
                String Username = binding.etIdHubungkan.getText().toString();
                String Password = binding.etPassword.getText().toString();

                if (!sqLiteHelper.isUsernameExists(Username)) {
                    sqLiteHelper.addUser(new UserModel(null, Username, Password));
                    this.snack(SNACK_SUCCESS, "Register Berhasil!, Silahkan Daftar.");

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.etIdHubungkan.setText(null);
                            binding.etPassword.setText(null);
                        }
                    }, Snackbar.LENGTH_LONG);
                } else {
                    this.snack(SNACK_ERROR, "Akun telah terdaftar");
                }
            }
        });

        binding.btnLogin.setOnClickListener(view -> {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        });

        binding.btnBack.setOnClickListener(view -> {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        });
    }

    private boolean validation() {
        boolean validate = false;

        String Username = binding.etIdHubungkan.getText().toString();
        String Password = binding.etPassword.getText().toString();

        if (Username.isEmpty()) {
            validate = false;
            binding.etIdHubungkan.setError("Harap isi Username anda!!!");
        } else {
            if (Username.length() > 5) {
                validate = true;
                binding.etIdHubungkan.setError(null);
            } else {
                validate = false;
                binding.etIdHubungkan.setError("Username anda terlalu singkat");
            }
        }

        if (Password.isEmpty()) {
            validate = false;
            binding.etIdHubungkan.setError("Harap isi Password anda!!!");
        } else {
            if (Password.length() > 5) {
                validate = true;
                binding.etIdHubungkan.setError(null);
            } else {
                validate = false;
                binding.etIdHubungkan.setError("Password anda terlalu singkat");
            }
        }

        return validate;
    }

    private void snack(String type, String msg) {
        if (type.equals(SNACK_SUCCESS)) {
            Snacky.builder()
                    .setActivity(this)
                    .setText(msg)
                    .setDuration(Snacky.LENGTH_LONG)
                    .success()
                    .show();

        } else {
            Snacky.builder()
                    .setActivity(this)
                    .setText(msg)
                    .setDuration(Snacky.LENGTH_LONG)
                    .error()
                    .show();
        }
    }
}