package com.example.belajarilmufalak.views.model;

public class MenuModel {

    private String title;
    private int images;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }
}
