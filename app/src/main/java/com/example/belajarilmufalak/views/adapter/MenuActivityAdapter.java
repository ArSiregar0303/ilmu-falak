package com.example.belajarilmufalak.views.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.belajarilmufalak.R;
import com.example.belajarilmufalak.databinding.ActivityMenuBinding;
import com.example.belajarilmufalak.databinding.ItemGridLayoutBinding;
import com.example.belajarilmufalak.views.main.CalenderActivity;
import com.example.belajarilmufalak.views.main.HilalActivity;
import com.example.belajarilmufalak.views.main.IlmuFalakActivity;
import com.example.belajarilmufalak.views.main.KiblatActivity;
import com.example.belajarilmufalak.views.model.MenuModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class MenuActivityAdapter extends RecyclerView.Adapter<MenuActivityAdapter.MenuActivityViewHolder> {

    Context context;
    private ArrayList<MenuModel> dataList;

    public MenuActivityAdapter(ArrayList<MenuModel> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @NotNull
    @Override
    public MenuActivityAdapter.MenuActivityViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_layout, parent, false);
        return new MenuActivityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MenuActivityAdapter.MenuActivityViewHolder holder, int position) {
        MenuModel menuModel = dataList.get(position);
        Glide.with(holder.itemView.getContext())
                .load(dataList.get(position).getImages())
                .apply(new RequestOptions().override(350, 550))
                .into(holder.img);

        holder.title.setText(menuModel.getTitle());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class MenuActivityViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView title;

        public MenuActivityViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id.imv_gameImage);
            title = itemView.findViewById(R.id.tv_gameName);

            itemView.setOnClickListener(view -> {
                switch (getAdapterPosition()) {

                    case 0:
                        itemView.getContext().startActivity(new Intent(itemView.getContext(), IlmuFalakActivity.class));
                        break;
                    case 1:
                        itemView.getContext().startActivity(new Intent(itemView.getContext(), KiblatActivity.class));
                        break;
                    case 2:
                        itemView.getContext().startActivity(new Intent(itemView.getContext(), HilalActivity.class));
                        break;
                    case 3:
                        itemView.getContext().startActivity(new Intent(itemView.getContext(), CalenderActivity.class));
                        break;
                }
            });
        }
    }
}
