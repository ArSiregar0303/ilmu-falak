package com.example.belajarilmufalak.views.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.transition.TransitionManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.belajarilmufalak.R;
import com.example.belajarilmufalak.databinding.ActivityTentangBinding;

public class TentangActivity extends AppCompatActivity {

    private ActivityTentangBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTentangBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        initialView();
    }

    private void initialView() {
        binding.back.setOnClickListener( view -> {
            startActivity(new Intent(this, MenuActivity.class));
            finish();   
        });

        binding.whatsaapLayout.setOnClickListener( view -> {
            TransitionManager.beginDelayedTransition(binding.container);
            if (binding.lyWhatsaap.getVisibility() == View.GONE) {
                binding.lyWhatsaap.setVisibility(View.VISIBLE);
                binding.ivWhatsaap.setRotation(90F);
            } else {
                binding.lyWhatsaap.setVisibility(View.GONE);
                binding.ivWhatsaap.setRotation(0F);
            }
        });

        binding.instagramLayout.setOnClickListener( view -> {
            TransitionManager.beginDelayedTransition(binding.container);
            if (binding.lyInstagram.getVisibility() == View.GONE) {
                binding.lyInstagram.setVisibility(View.VISIBLE);
                binding.ivInstagram.setRotation(90F);
            } else {
                binding.lyInstagram.setVisibility(View.GONE);
                binding.ivInstagram.setRotation(0F);
            }
        });

        binding.twitterLayout.setOnClickListener( view -> {
            TransitionManager.beginDelayedTransition(binding.container);
            if (binding.lyTwitter.getVisibility() == View.GONE) {
                binding.lyTwitter.setVisibility(View.VISIBLE);
                binding.ivTwitter.setRotation(90F);
            } else {
                binding.lyTwitter.setVisibility(View.GONE);
                binding.ivTwitter.setRotation(0F);
            }
        });
    }
}